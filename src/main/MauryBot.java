package main;

import java.util.Random;

import org.jibble.pircbot.PircBot;

public class MauryBot extends PircBot
{

	public MauryBot()
	{
		this.setName("MauryBot"); // Username of the bot
	}

	public void onMessage(String channel, String sender, String login, String hostname, String message)
	{
		String modifying = message.replaceAll("\\s+", " ");
		sender = sender.toLowerCase();

		if(modifying.equalsIgnoreCase("am i the father?") & sender.equals("endsgamer")) // no spammy.
		{
			Random rand = new Random();
			int result = rand.nextInt(2);

			if(result == 1)
			{
				sendMessage(channel, "The results are in, you are NOT the father!");
			}
			else if(result == 0)
			{
				sendMessage(channel, "The results are in, you ARE the father!");
			}

			return;
		}

		if(modifying.equalsIgnoreCase("bye maury"))
		{
			sendMessage(channel, "Committing sudoku");

			try
			{
				Thread.sleep(2000); // Let it send its final message
			}
			catch (InterruptedException ex)
			{
				ex.printStackTrace();
			}

			quitServer();
			System.exit(0);

			return;
		}
	}
}
