package main;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class MauryBotMain
{
	
	private static String config = "configuration.txt";
	
	public static void main(String[] args) throws Exception
	{
		MauryBot bot = new MauryBot();
		
		//bot.setVerbose(true);
				
		bot.connect("irc.twitch.tv", 6667, getOAuth());
				
		bot.joinChannel("#coestar");
	}
	
	
	private static String getOAuth() throws IOException
	{
		String oauth = null;
		
		FileReader input = new FileReader(config);
		Scanner s = new Scanner(input);
		
		oauth = s.nextLine();
		
		s.close();
		input.close();
		
		return oauth;
	}
}
